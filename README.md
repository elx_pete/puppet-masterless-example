# README #

This will describe the steps needed to run puppet masterless on a RHEL node.  You could run it on any unix flavor puppet supports.
I specify unix since my sample module add a cron entry.

### What is this repository for? ###

* This is a puppet masterless example
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* You will need a UNIX/Linux box with root access.
#### Ensure Puppet is installed ####
* Option 1: rpm install
	* rpm -ivh https://yum.puppetlabs.com/puppetlabs-release-el-7.noarch.rpm
	* yum install puppet
* Option 2: vagrant up an image with puppet in it 

#### Ensure git is installed ####  
* which git

#### Ensure the ssh key exists for the machine and git user ####
* cd ~
* mkdir .ssh
* touch ~/.ssh/id_rsa
* chmod 600 ~/.ssh/id_rsa 
* place the following text (key) into ~/.ssh/id_rsa
~~~~
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAsdm1APb+dpexsvl7wcSPA/9bTQfsq+lOVRrMgGdkMk1IlbAT
n3pi3hAZ9mV/8ptAmubO3+GC7R/9zFINvo+hjRrUMZhO93DLKTS3q6aa9DzMyUdP
kqnvfh+WJchdVePnzmda1ySrcG85huGZIucAyHfZDcrezl7SlqsqZpy3PY+6iwKb
eLxEHI8OKImIkFXfjddTZic/R4cNeQN942nDAlMm9bVRs16JFtfu5jKi3uh8O2Te
Hix4HzuAQjjXPB0n22TRFoYr9U3owOWYLrhzVMq2JuOvxJ0TeMOOy4HlVUwVvw3n
agtNZLFV2kJ/tj/a5NpZd/StVEblYtqWNj6GhwIDAQABAoIBADiW8i4VaVBo4iT1
lzhHh84Y0X9INzlYKhGQTmPJAiuzRZZDiWrkMVNBQlGRaE3UspDXQhogkl1LIaGC
tlbdMuVIFR6WvQGkxTAeUxbuEz4Fno+o/hvHsKmr+UWaZIBw3RBi+JeeVmKXMXHG
lMhzQl+HKuxlkdGOgGlqmzo9QVz3t25kbAkt/HUvL359oWi419lamVemMfCMT4oO
QE7JG+4JxSUs46rNaoJmej82igK+pF4BckCCy+5XEV1E9iU2671aLxZ+72g7uzrU
l/QijAzk5hrLHfTWAcNtmyLDb/o+V8cbHIK2tyxA3+tPenai59lvLa7U3WFd1F1P
9YhrBHECgYEA6PSIICfRjziHEEKGh8H7x+PzqxtTrIe+CV+QD8csji0YK6yZo5ui
3oS3skAEJXsQtt/jrK3AYjuWVOtG2RENyXxB3rlc1bWZbYl3aM14gIeAP2Nn7vPg
xRd4k8az1uNwTQDC93Eb/wuTcZTkAM/Yzdx0SvfHHBN2dsO4qzdvAW8CgYEAw3Gs
8dyr1M6stVYq4M81SwNwsUmwwAcYh31F4VLwClSHN0GrDuVfzE8CzWqYMwD166CZ
PUx2yPz1i+0hgn5Ugov+1h6snBl8oVcUx1QPDJn9BjrCH+i2O/jtNNYMnsUpjhGg
WV4W8q43XPVF3ztKFbuMSLDVwGqTcjqoiwBgEGkCgYEA3fxfV1z6WeNA+WKbdfSJ
LDLxeMTimE8F7UKuMgrXT2hzVvLzuS9PJYIM1oQgL2M1Sz7QYzVzVWh42dUVvzV8
ENsinc2/RDr3yKStMCZURQ5aTI06dj7ZmpbCJPe9WfGDiy1FnKuhRQnP5/U8y/VE
IO+JtgKlLwDw6z2Y1TdOxWsCgYBsoqcK2Sn549dEBF1ZEFQDGiptoRRJ6KfNx+gr
0rnyRAHUyP+YDA8vtUtNes6WqxYGnwIOfI4aMeEaofhaXHYFCmPgCLTRH6mS1KCM
G0EyM/XcnJSCwAtrUnYnRENHjTgH6+xBQHdo9UV4uizYZFBNvC3ByelGRvcmK0y7
1r2O4QKBgGrYo2dwLsRcl5+3ywZouYKM9uV2FXxZmmr+Tn6QCs2WL7sA8O/iNAvp
cyiyQbfe/N2P6n+6Fkrgqut4H/+or+eVzMmtrW6UelbahIe/E9THQK5CDru9KDYL
htiREWNz6fz2+WttRYBqQZnJCn0TAlKlmowLzu/aCaYFSdI8wmZz
-----END RSA PRIVATE KEY-----
~~~~

#### clone the Git repository with puppet modules ####
   
* git clone git@bitbucket.org:elx_pete/puppet-masterless-example.git
* ln -s puppet-masterless-example /etc/puppet

#### Apply ####
* puppet apply /etc/puppet/manifests/site.pp

### Contribution guidelines ###

* Feel free to add modules and update the site.pp file

### Who do I talk to? ###

* Ed Nystrom (ed.nystrom@elyxor.com)