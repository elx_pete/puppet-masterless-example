class dir-check {
    file { '/var/kcg':
        ensure  => directory,
        mode    => 0755,
        owner   => root,
        group   => root,
    }
}
